package jo.training.jenskinsdockercicd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JenskinsDockerCicdApplication {

  public static void main(String[] args) {
    SpringApplication.run(JenskinsDockerCicdApplication.class, args);
  }

}
